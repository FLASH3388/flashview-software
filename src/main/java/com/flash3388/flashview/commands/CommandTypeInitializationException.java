package com.flash3388.flashview.commands;

public class CommandTypeInitializationException extends Exception {

    public CommandTypeInitializationException(Throwable cause) {
        super(cause);
    }
}
