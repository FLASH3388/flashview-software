package com.flash3388.flashview.configuration.loader;

public class ConfigurationLoadException extends Exception {

    public ConfigurationLoadException(Throwable cause) {
        super(cause);
    }
}
